package rwm.com.runwithme.viewmodel;

import android.content.Context;
import android.content.res.Resources;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import rwm.com.runwithme.R;
import rwm.com.runwithme.model.RunStat;
import rwm.com.runwithme.model.RunSummary;

public class RunSummaryViewModel {
    private final RunSummary runSummary;
    private final Resources resources;

    public RunSummaryViewModel(RunSummary runSummary, Resources resources) {
        this.runSummary = runSummary;
        this.resources = resources;
    }

    public long getId() {
        return runSummary.getId();
    }

    public long getDistance() {
        return runSummary.getDistance();
    }

    public int getElevation() {
        return runSummary.getElevation();
    }

    public long getDuration() {
        return runSummary.getDuration();
    }

    public long getStartTime() {
        return runSummary.getStartTime();
    }

    public float getAvgSpeed() {
        return runSummary.getAvgSpeed();
    }

    public float getAvgPace() {
        return runSummary.getAvgPace();
    }

    public float getMaxSpeed() {
        return runSummary.getMaxSpeed();
    }

    public int getCalories() {
        return runSummary.getCalories();
    }

    public ArrayList<RunStat> getRunStats() {
        return runSummary.getRunStats();
    }

    public String getDistanceText() {
        return String.format("%d %s", getDistance(), resources.getString(R.string.unit_meter));
    }

    public String getCaloriesText() {
        return String.format("%d %s", getCalories(), resources.getString(R.string.unit_calorie));
    }

    public String timeElapsed() {
        long currentTime = new Date().getTime();
        long elapsedTimeSeconds = (currentTime - runSummary.getStartTime()) / 1000;

        if (elapsedTimeSeconds >= 3 * RunSummary.DAY)
            return DateFormat.getDateInstance().format((new Date(runSummary.getStartTime())));
        else if (elapsedTimeSeconds >= RunSummary.DAY)
            return pluralize(elapsedTimeSeconds / RunSummary.DAY, resources.getString(R.string.unit_day));
        else if (elapsedTimeSeconds >= RunSummary.HOUR)
            return pluralize(elapsedTimeSeconds / RunSummary.HOUR, resources.getString(R.string.unit_hour));
        else if (elapsedTimeSeconds >= RunSummary.MINUTE)
            return pluralize(elapsedTimeSeconds / RunSummary.MINUTE, resources.getString(R.string.unit_minute));
        return "Just now";
    }

    private String pluralize(long time, String unit) {
        if (time == 1)
            return String.format("%d %s %s", time, unit, resources.getString(R.string.ago));
        return String.format("%d %s%s %s", time, unit, "s", resources.getString(R.string.ago));
    }

    public String timerFormatDuration() {
        long hours = runSummary.getDuration() / (1000 * 60 * 60);
        long minutes = (runSummary.getDuration() / (1000 * 60)) % 60;
        long seconds = (runSummary.getDuration() / 1000) % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public long getMinutes() {
        return runSummary.getDuration() / (1000 * 60);
    }

    public void saveSummary(Context context) {
        runSummary.save(context);
    }

    public void deleteSummary(Context context) {
        runSummary.delete(context);
    }
}
