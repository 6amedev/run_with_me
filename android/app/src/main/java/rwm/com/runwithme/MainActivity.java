package rwm.com.runwithme;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.parse.ParseUser;

import java.util.List;

import bolts.Continuation;
import bolts.Task;
import rwm.com.runwithme.model.RunSummary;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        HistoryListFragment.HistoryListCallback {

    public static final String USER_PREFERENCES = "user-preferences";
    public static final String USERNAME_KEY = "username";
    public static final String USER_LOCATION_KEY = "user-location";
    public static final String USER_WEIGHT_KEY = "user-weight";
    public static final String USER_HEIGHT_KEY = "user-height";
    public static final String USER_EMAIL_KEY = "user-email";
    public static final String USER_IMAGE_PATH_KEY = "user-profile-picture";
    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        populateUserInfo(navigationView);
        showLanding();
    }

    @Override
    public void onPause() {
        if (checkPlayServices() && !isRunServiceRunning())
            stopService(new Intent(this, LocationService.class));
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPlayServices() && !isRunServiceRunning())
            startService(new Intent(this, LocationService.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (currentFragment.getClass() != HomeFragment.class) {
            showLanding();
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {
            showLanding();
        } else if (id == R.id.history) {
            showHistory();
        } else if (id == R.id.profile) {
            showProfile();
        } else if (id == R.id.action_logout) {
            final ProgressDialog dialog = ProgressDialog.show(this, null, "Logging Out", true, false);
            Task.forResult(ParseUser.getCurrentUser())
                    .onSuccess(new Continuation<ParseUser, Void>() {
                        @Override
                        public Void then(Task<ParseUser> task) throws Exception {
                            ParseUser.logOut();
                            return null;
                        }
                    }, Task.BACKGROUND_EXECUTOR)
                    .continueWith(new Continuation<Void, Void>() {
                        @Override
                        public Void then(Task<Void> task) throws Exception {
                            dialog.dismiss();
                            if (task.isFaulted()) {
                                Log.e("APP_LOG", "Failed to logout user");
                            } else {
                                startActivity(new Intent(MainActivity.this, DispatchLoginActivity.class));
                                finish();
                            }
                            return null;
                        }
                    }, Task.UI_THREAD_EXECUTOR);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void populateUserInfo(NavigationView navigationView) {
        String imagePath = getString(R.string.default_profile_picture);
        SharedPreferences sharedPreferences = getSharedPreferences(USER_PREFERENCES, MODE_PRIVATE);

        TextView username = (TextView) navigationView.getHeaderView(0).findViewById(R.id.username);
        TextView userLocation = (TextView) navigationView.getHeaderView(0).findViewById(R.id.location);
        ImageView userProfilePicture = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.profile_image);

        username.setText(sharedPreferences.getString(USERNAME_KEY, "Guest"));
        userLocation.setText(sharedPreferences.getString(USER_LOCATION_KEY, "Location"));
        userProfilePicture.setImageURI(Uri.parse(sharedPreferences.getString(USER_IMAGE_PATH_KEY, imagePath)));
    }

    public void showLanding() {
        currentFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putBoolean(RunFragment.RUNNING_KEY, isRunServiceRunning());
        currentFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, currentFragment)
                .commit();
    }

    private void showHistory() {
        currentFragment = new HistoryListFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, currentFragment)
                .commit();
    }

    private void showProfile() {
        currentFragment = new ProfileFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, currentFragment)
                .commit();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this,
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void click(RunSummary runSummary) {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putExtra(HistoryActivity.HISTORY_KEY, runSummary);
        startActivity(intent);
    }

    private boolean isRunServiceRunning() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServices) {
            if (runningServiceInfo.service.getClassName().equals(RunService.class.getName())) {
                return true;
            }
        }
        return false;
    }
}
