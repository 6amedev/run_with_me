package rwm.com.runwithme;

import com.parse.ui.ParseLoginDispatchActivity;

public class DispatchLoginActivity extends ParseLoginDispatchActivity {
    @Override
    protected Class<?> getTargetClass() {
        return MainActivity.class;
    }
}
