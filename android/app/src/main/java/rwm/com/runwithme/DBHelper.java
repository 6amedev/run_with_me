package rwm.com.runwithme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import rwm.com.runwithme.model.RunSummary;

public class DBHelper extends SQLiteOpenHelper {

    public static DBHelper dbHelper;

    public DBHelper(Context context) {
        super(context, RunSummary.TABLE_NAME, null, 1);
    }

    public static DBHelper getInstance(Context context) {
        if (dbHelper == null)
            dbHelper = new DBHelper(context);
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSql = "CREATE TABLE " + RunSummary.TABLE_NAME + "(" +
                RunSummary._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                RunSummary.DISTANCE + " INTEGER," +
                RunSummary.ELEVATION + " INTEGER," +
                RunSummary.DURATION + " INTEGER," +
                RunSummary.START_TIME + " INTEGER," +
                RunSummary.AVG_SPEED + " REAL," +
                RunSummary.AVG_PACE + " REAL," +
                RunSummary.MAX_SPEED + " REAL," +
                RunSummary.CALORIES + " INTEGER);";
        db.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long addSummary(RunSummary runSummary) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(RunSummary.DISTANCE, runSummary.getDistance());
        values.put(RunSummary.ELEVATION, runSummary.getElevation());
        values.put(RunSummary.DURATION, runSummary.getDuration());
        values.put(RunSummary.START_TIME, runSummary.getStartTime());
        values.put(RunSummary.AVG_SPEED, runSummary.getAvgSpeed());
        values.put(RunSummary.AVG_PACE, runSummary.getAvgPace());
        values.put(RunSummary.MAX_SPEED, runSummary.getMaxSpeed());
        values.put(RunSummary.CALORIES, runSummary.getCalories());
        return db.insert(RunSummary.TABLE_NAME, null, values);
    }

    public Cursor getAllRows() {
        String selectQuery = "SELECT  * FROM " + RunSummary.TABLE_NAME
                + " ORDER BY " + RunSummary._ID + " DESC;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public void deleteSummary(long ID) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(RunSummary.TABLE_NAME, RunSummary._ID + "=" + ID, null);
    }
}