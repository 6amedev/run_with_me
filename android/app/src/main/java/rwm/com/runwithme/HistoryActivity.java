package rwm.com.runwithme;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

import rwm.com.runwithme.model.RunSummary;
import rwm.com.runwithme.viewmodel.RunSummaryViewModel;

public class HistoryActivity extends AppCompatActivity {

    public final static String HISTORY_KEY = "history_key";

    private TextView durationView;
    private TextView distanceView;
    private TextView caloriesView;
    private TextView elevationView;
    private TextView startTimeView;
    private TextView avgSpeedView;
    private TextView avgPaceView;
    private TextView maxSpeedView;
    private RunSummaryViewModel runSummaryViewModel;
    RunSummary runSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        runSummary = getIntent().getExtras().getParcelable(HISTORY_KEY);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, getResources());
        findViews();
        displaySummary();

        overridePendingTransition(R.anim.slide_in_right, R.anim.zoom_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.discard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.discard_run:
                discardSummary();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void discardSummary() {
//        runSummaryViewModel.deleteSummary(this);
        Toast.makeText(this, "Get ID", Toast.LENGTH_SHORT).show();
//        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.zoom_in, R.anim.slide_out_right);
    }

    private void findViews() {
        durationView = ((TextView) findViewById(R.id.summary_duration));
        distanceView = (TextView) findViewById(R.id.summary_distance);
        caloriesView = (TextView) findViewById(R.id.summary_calories);
        startTimeView = (TextView) findViewById(R.id.summary_start_time);
        avgSpeedView = (TextView) findViewById(R.id.summary_avg_speed);
        avgPaceView = (TextView) findViewById(R.id.summary_avg_pace);
        maxSpeedView = (TextView) findViewById(R.id.summary_max_speed);
        elevationView = (TextView) findViewById(R.id.summary_elevation);
    }

    private void displaySummary() {
        durationView.setText(runSummaryViewModel.timerFormatDuration());
        distanceView.setText(String.valueOf(runSummaryViewModel.getDistance()));
        caloriesView.setText(String.valueOf(runSummaryViewModel.getCalories()));
        startTimeView.setText(String.valueOf(DateFormat.getTimeInstance().format(new Date(runSummaryViewModel.getStartTime()))));
        avgSpeedView.setText(String.format("%.2f", runSummaryViewModel.getAvgSpeed()));
        avgPaceView.setText(String.format("%.2f", runSummaryViewModel.getAvgPace()));
        maxSpeedView.setText(String.format("%.2f", runSummaryViewModel.getMaxSpeed()));
        elevationView.setText(String.valueOf(runSummaryViewModel.getElevation()));
    }
}
