package rwm.com.runwithme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import rwm.com.runwithme.model.RunStat;
import rwm.com.runwithme.model.RunSummary;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private int accuracyColor = 0x2cff1e00;
    private GoogleMap googleMap;
    private LatLng lastLatLong;
    private Marker marker;
    private Circle accuracyCircle;
    private ToggleButton runButton;
    private Context context;
    private RunStat runStat;
    private ArrayList<RunStat> runStats;
    private BroadcastReceiver runServiceRecevier;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        context = getActivity().getApplicationContext();

        Boolean isRunning = getArguments().getBoolean(RunFragment.RUNNING_KEY);
        runButton = ((ToggleButton) view.findViewById(R.id.btn_map_toggle));
        runButton.setChecked(isRunning);

        lastLatLong = new LatLng(-34, 151);         // Sydney
        Location lastLocation = LocationService.getLastLocation();
        if (lastLocation != null)
            lastLatLong = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

        runServiceRecevier = new RunBroadcastReceiver();
        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleRun();
            }
        });

        setHasOptionsMenu(true);

        return view;
    }

    private void toggleRun() {
        if (runButton.isChecked()) {
            getActivity().startService(new Intent(context, RunService.class));
        } else {
            getActivity().stopService(new Intent(context, RunService.class));
            Intent intent = new Intent(context, RunSummaryActivity.class);
            RunSummary runSummary = new RunSummary(runStat.getDistance(),
                    runStat.getElevation(), runStat.getDuration(), runStat.getStartTime(), runStats);
            intent.putExtra(RunSummaryActivity.RUN_SUMMARY_KEY, runSummary);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_map_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.toggle_to_run) {
            ((MapFragmentCallback) getActivity()).toggleShowRun();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupMap(googleMap);
    }

    private void updateMap(Double latitude, Double longitude, float accuracy) {
        LatLng latLong = new LatLng(latitude, longitude);
        if (googleMap != null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLong, 18));
            drawRoute(latLong);
            marker.setPosition(latLong);
            accuracyCircle.setCenter(latLong);
            accuracyCircle.setRadius(accuracy);
        }
        lastLatLong = latLong;
    }

    private void drawRoute(LatLng latLong) {
        if (lastLatLong != null) {
            googleMap.addPolyline(new PolylineOptions()
                    .add(latLong, lastLatLong)
                    .geodesic(true));
        }
    }

    private void setupMap(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLatLong, 18));
        marker = googleMap.addMarker((new MarkerOptions().position(lastLatLong)
                .title("Current location").icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_marker))));
        accuracyCircle = googleMap.addCircle(new CircleOptions().center(lastLatLong)
                .radius(0).fillColor(accuracyColor).strokeWidth(0));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(runServiceRecevier);
        runButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_out_bottom));
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(runServiceRecevier,
                        new IntentFilter(RunService.BROADCAST_KEY));
        runButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_bottom));
    }

    public interface MapFragmentCallback {
        void toggleShowRun();
    }

    private class RunBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            runStat = intent.getExtras()
                    .getParcelable(RunService.RUN_STAT_KEY);
            runStats = intent.getExtras()
                    .getParcelableArrayList(RunService.RUN_STATS_KEY);
            updateMap(runStat.getLatitude(), runStat.getLongitude(), runStat.getAccuracy());
        }
    }
}
