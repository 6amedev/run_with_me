package rwm.com.runwithme;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HomeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        skipToRun();
        loadHistory();

        FloatingActionButton runButton = (FloatingActionButton) view.findViewById(R.id.start_run);
        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startService(new Intent(getActivity(), RunService.class));
                startActivity(new Intent(getActivity(), RunActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(R.string.app_name);
    }

    private void skipToRun() {
        Boolean isRunning = getArguments().getBoolean(RunFragment.RUNNING_KEY);
        if (isRunning)
            startActivity(new Intent(getActivity(), RunActivity.class));
    }

    private void loadHistory() {
        HistoryListFragment historyListFragment = new HistoryListFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.container_history_list, historyListFragment)
                .commit();
    }
}
