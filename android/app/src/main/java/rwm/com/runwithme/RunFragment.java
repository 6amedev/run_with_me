package rwm.com.runwithme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

import rwm.com.runwithme.model.RunStat;
import rwm.com.runwithme.model.RunSummary;

public class RunFragment extends Fragment {

    public final static String RUNNING_KEY = "is_running_key";

    private Context context;
    private View view;

    private TextView durationView;
    private TextView distanceView;
    private TextView speedView;
    private TextView elevationView;
    private TextView paceView;
    private ToggleButton runButton;
    private BroadcastReceiver runServiceReceiver;
    private RunStat runStat;
    private ArrayList<RunStat> runStats;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_run, container, false);
        context = getActivity().getApplicationContext();
        getActivity().setTitle(R.string.app_name);

        findViews();
        Boolean isRunning = getArguments().getBoolean(RunFragment.RUNNING_KEY);
        runButton.setChecked(isRunning);

        runServiceReceiver = new RunBroadcastReceiver();
        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleRun();
            }
        });

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_run_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.toggle_to_map) {
            ((RunFragmentCallback) getActivity()).toggleShowMap();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(runServiceReceiver);
        super.onPause();
        runButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_out_bottom));
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(context)
                .registerReceiver(runServiceReceiver,
                        new IntentFilter(RunService.BROADCAST_KEY));
        runButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_in_bottom));
    }

    private void findViews() {
        durationView = ((TextView) view.findViewById(R.id.duration));
        distanceView = ((TextView) view.findViewById(R.id.distance));
        speedView = ((TextView) view.findViewById(R.id.speed));
        elevationView = ((TextView) view.findViewById(R.id.elevation));
        paceView = ((TextView) view.findViewById(R.id.pace));
        runButton = (ToggleButton) view.findViewById(R.id.btn_run_toggle);
    }

    private void toggleRun() {
        if (runButton.isChecked()) {
            getActivity().startService(new Intent(context, RunService.class));
        } else {
            getActivity().stopService(new Intent(context, RunService.class));
            Intent intent = new Intent(context, RunSummaryActivity.class);
            RunSummary runSummary = new RunSummary(runStat.getDistance(),
                    runStat.getElevation(), runStat.getDuration(), runStat.getStartTime(), runStats);
            intent.putExtra(RunSummaryActivity.RUN_SUMMARY_KEY, runSummary);
            startActivity(intent);
            getActivity().finish();
        }
    }

    private void displayStats() {
        long hours = runStat.getDuration() / (1000 * 60 * 60);
        long minutes = (runStat.getDuration() / (1000 * 60)) % 60;
        long seconds = (runStat.getDuration() / 1000) % 60;

        distanceView.setText(String.valueOf(runStat.getDistance()));
        speedView.setText(String.format("%.2f", runStat.getSpeed()));
        paceView.setText(String.format("%.2f", runStat.getPace()));
        elevationView.setText(String.valueOf(runStat.getElevation()));
        durationView.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
    }

    private class RunBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            runStat = intent.getExtras()
                    .getParcelable(RunService.RUN_STAT_KEY);
            runStats = intent.getExtras()
                    .getParcelableArrayList(RunService.RUN_STATS_KEY);
            displayStats();
        }
    }

    public interface RunFragmentCallback {
        void toggleShowMap();
    }
}
