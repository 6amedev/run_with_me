package rwm.com.runwithme;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rwm.com.runwithme.model.RunSummary;
import rwm.com.runwithme.viewmodel.RunSummaryViewModel;

public class HistoryListAdapter extends BaseAdapter {

    Context context;
    DBHelper dbHelper;
    Cursor cursor;
    LayoutInflater layoutInflater;

    public HistoryListAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.dbHelper = DBHelper.getInstance(context);
        this.cursor = dbHelper.getAllRows();
    }

    @Override
    public int getCount() {
        return cursor.getCount();
    }

    @Override
    public RunSummary getItem(int position) {
        cursor.moveToPosition(position);
        return new RunSummary(
                cursor.getInt(cursor.getColumnIndexOrThrow(RunSummary._ID)),
                cursor.getLong(cursor.getColumnIndexOrThrow(RunSummary.DISTANCE)),
                cursor.getInt(cursor.getColumnIndexOrThrow(RunSummary.ELEVATION)),
                cursor.getLong(cursor.getColumnIndexOrThrow(RunSummary.DURATION)),
                cursor.getLong(cursor.getColumnIndexOrThrow(RunSummary.START_TIME)),
                cursor.getFloat(cursor.getColumnIndexOrThrow(RunSummary.AVG_SPEED)),
                cursor.getFloat(cursor.getColumnIndexOrThrow(RunSummary.AVG_PACE)),
                cursor.getFloat(cursor.getColumnIndexOrThrow(RunSummary.MAX_SPEED)),
                cursor.getInt(cursor.getColumnIndexOrThrow(RunSummary.CALORIES))
        );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.history_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.distanceView = (TextView) view.findViewById(R.id.history_distance);
            viewHolder.caloriesView = (TextView) view.findViewById(R.id.history_calories);
            viewHolder.timestampView = (TextView) view.findViewById(R.id.history_timestamp);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        ViewHolder viewHolder = ((ViewHolder) view.getTag());
        RunSummary runSummary = getItem(position);
        RunSummaryViewModel runSummaryViewModel = new RunSummaryViewModel(runSummary, view.getResources());
        viewHolder.distanceView.setText(runSummaryViewModel.getDistanceText());
        viewHolder.caloriesView.setText(runSummaryViewModel.getCaloriesText());
        viewHolder.timestampView.setText(runSummaryViewModel.timeElapsed());
        return view;
    }

    public void deleteSummary(int ID) {
        RunSummaryViewModel runSummaryViewModel = new RunSummaryViewModel(getItem(ID), context.getResources());
        runSummaryViewModel.deleteSummary(context);
        refreshDataSet();
    }

    public void refreshDataSet() {
        safeCloseCursor();
        cursor = dbHelper.getAllRows();
        notifyDataSetChanged();
    }

    private void safeCloseCursor() {
        if (cursor != null && !cursor.isClosed()) cursor.close();
    }

    static class ViewHolder {
        TextView distanceView;
        TextView caloriesView;
        TextView timestampView;
    }
}
