package rwm.com.runwithme;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import rwm.com.runwithme.model.RunStat;

public class RunService extends Service {
    public static final String BROADCAST_KEY = "run_broadcast_key";
    public static final String RUN_STAT_KEY = "run_stat_key";
    private static final float ACCURACY_THRESHOLD = 20;
    private static final double SPEED_THRESHOLD = 0.1;
    private static final float PACE_FACTOR = 26.82F;
    public static final String RUN_STATS_KEY = "run_stats_key";
    public static final int UPDATE_INTERVAL = 1000;

    private float pace;
    private float speed;
    private Timer timer;
    private long distance;
    private long startTime;
    private double latitude;
    private int elevation;
    private float accuracy;
    private double longitude;
    private double startAltitude;
    private ArrayList<RunStat> runStats;
    private Location lastLocation;
    private Boolean running = false;
    private BroadcastReceiver locationRecevier;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!running) {
            initializeStats();
            startRun();
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationRecevier);
        running = false;
        super.onDestroy();
    }

    private void initializeStats() {
        Location location = LocationService.lastLocation;
        if (location != null) {
            accuracy = location.getAccuracy();
            if (location.hasSpeed()) {
                speed = location.getSpeed();
                if (speed > 0)
                    pace = PACE_FACTOR / speed;
            }
            if (location.hasAltitude())
                startAltitude = location.getAltitude();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            lastLocation = location;
        }
    }

    private void startRun() {
        running = true;
        locationRecevier = new LocationBroadcastReceiver();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(locationRecevier,
                        new IntentFilter(LocationService.BROADCAST_KEY));

        startTime = new Date().getTime();
        runStats = new ArrayList<>();
        timer = new Timer();
        timer.scheduleAtFixedRate(new RunTask(startTime), 0, UPDATE_INTERVAL);
        runNotify(getString(R.string.app_name), getString(R.string.started_running));
    }

    private void runNotify(String title, String content) {
        Intent intent = new Intent(this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentIntent(pendingIntent);

        int notificationId = 1;
        startForeground(notificationId, notificationBuilder.build());
    }

    public void sendStats(long duration) {
        RunStat runStat = new RunStat(pace, speed, distance, duration,
                startTime, latitude, longitude, elevation, accuracy);
        if ((duration / 1000) % 60 == 0) {
            runStats.add(runStat);
        }
        Intent intent = new Intent(BROADCAST_KEY);
        intent.putExtra(RUN_STAT_KEY, runStat);
        intent.putParcelableArrayListExtra(RUN_STATS_KEY, runStats);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void calculateStats(Location location) {
        accuracy = location.getAccuracy();
        if (accuracy < ACCURACY_THRESHOLD) {
            if (location.hasSpeed()) {
                speed = location.getSpeed();
                if (speed > 0)
                    pace = PACE_FACTOR / speed;
            }
            if ((lastLocation != null) && (speed > SPEED_THRESHOLD))
                distance += location.distanceTo(lastLocation);
            if (location.hasAltitude()) {
                if (startAltitude == 0)
                    startAltitude = location.getAltitude();
                else
                    elevation = (int) (location.getAltitude() - startAltitude);
            }
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            lastLocation = location;
        }
    }

    private class LocationBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location currentLocation = intent.getExtras()
                    .getParcelable(LocationService.LOCATION_KEY);
            calculateStats(currentLocation);
        }
    }

    private class RunTask extends TimerTask {
        private final long startTime;

        public RunTask(long startTime) {
            this.startTime = startTime;
        }

        @Override
        public void run() {
            long currentTime = new Date().getTime();
            long duration = currentTime - startTime;
            sendStats(duration);
        }
    }
}
