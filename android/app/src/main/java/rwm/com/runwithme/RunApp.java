package rwm.com.runwithme;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;

public class RunApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this);
        ParseFacebookUtils.initialize(this);
    }
}
