package rwm.com.runwithme;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileFragment extends Fragment {

    public static final int PICK_IMAGE_REQUEST = 1;

    View view;

    EditText firstNameInput;
    EditText lastNameInput;
    EditText weightInput;
    EditText heightInput;
    EditText emailInput;
    EditText locationInput;
    ImageView imageView;
    FloatingActionButton imageInput;
    FloatingActionButton doneButton;
    TextView usernameView;
    TextView locationView;
    ImageView profilePictureView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setTitle(R.string.profile);

        findViews();

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
                Toast.makeText(getActivity().getApplicationContext(), "Saved!", Toast.LENGTH_SHORT).show();
            }
        });

        imageInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        final ScrollView scrollview = ((ScrollView) getActivity().findViewById(R.id.profile_scrollview));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String[] projection = new String[]{MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String imagePath = "file://" + cursor.getString(columnIndex);

            imageView.setImageURI(uri);
            imageView.setTag(imagePath);
        }
    }

    private void validateData() {
        if (firstNameInput.getText().toString().trim().isEmpty())
            firstNameInput.setError("Can't be empty.");
        else if (weightInput.getText().toString().trim().isEmpty())
            weightInput.setError("Can't be empty.");
        else if (heightInput.getText().toString().trim().isEmpty())
            heightInput.setError("Can't be empty.");
        else if (emailInput.getText().toString().trim().isEmpty())
            emailInput.setError("Can't be empty.");
        else
            updateUserInfo();
    }

    private void updateUserInfo() {
        String imagePath = (String) imageView.getTag();
        String username = firstNameInput.getText() + " " + lastNameInput.getText();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(MainActivity.USER_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit()
                .putString(MainActivity.USERNAME_KEY, username)
                .putString(MainActivity.USER_LOCATION_KEY, String.valueOf(locationInput.getText()))
                .putInt(MainActivity.USER_WEIGHT_KEY, Integer.parseInt(weightInput.getText().toString()))
                .putInt(MainActivity.USER_HEIGHT_KEY, Integer.parseInt(heightInput.getText().toString()))
                .putString(MainActivity.USER_EMAIL_KEY, String.valueOf(emailInput.getText()))
                .putString(MainActivity.USER_IMAGE_PATH_KEY, imagePath)
                .commit();
        usernameView.setText(username);
        locationView.setText(locationInput.getText());
        profilePictureView.setImageURI(Uri.parse(imagePath));
    }

    private void findViews() {
        firstNameInput = (EditText) view.findViewById(R.id.input_first_name);
        lastNameInput = (EditText) view.findViewById(R.id.input_last_name);
        weightInput = (EditText) view.findViewById(R.id.input_weight);
        heightInput = (EditText) view.findViewById(R.id.input_height);
        emailInput = (EditText) view.findViewById(R.id.input_email);
        locationInput = (EditText) view.findViewById(R.id.input_location);
        imageInput = (FloatingActionButton) view.findViewById(R.id.input_image);
        imageView = (ImageView) view.findViewById(R.id.profile_picture_view);
        doneButton = (FloatingActionButton) view.findViewById(R.id.btn_done);
        usernameView = (TextView) getActivity().findViewById(R.id.username);
        locationView = (TextView) getActivity().findViewById(R.id.location);
        profilePictureView = (ImageView) getActivity().findViewById(R.id.profile_image);
    }
}
