package rwm.com.runwithme.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import java.util.ArrayList;

import rwm.com.runwithme.DBHelper;

public class RunSummary implements Parcelable, BaseColumns {

    public static final float SECONDS_FACTOR = 1000;
    public static final float MINUTES_FACTOR = 60000;
    public static final float MILES_FACTOR = 1609;
    public static final String TABLE_NAME = "run_summary";
    public static final String DISTANCE = "distance";
    public static final String ELEVATION = "elevation";
    public static final String DURATION = "duration";
    public static final String START_TIME = "start_time";
    public static final String AVG_SPEED = "avg_speed";
    public static final String AVG_PACE = "avg_pace";
    public static final String MAX_SPEED = "max_speed";
    public static final String CALORIES = "calories";
    public static final String[] ALL_COLS = {_ID, DISTANCE, ELEVATION, DURATION,
            START_TIME, AVG_SPEED, AVG_PACE, MAX_SPEED, CALORIES};
    public static final int MINUTE = 60;
    public static final int HOUR = 60 * 60;
    public static final int DAY = 24 * 60 * 60;

    long id;
    long distance;
    int elevation;
    long duration;
    long startTime;
    float avgSpeed;
    float avgPace;
    float maxSpeed;
    int calories;
    ArrayList<RunStat> runStats;

    public long getId() {
        return id;
    }

    public long getDistance() {
        return distance;
    }

    public int getElevation() {
        return elevation;
    }

    public long getDuration() {
        return duration;
    }

    public long getStartTime() {
        return startTime;
    }

    public float getAvgSpeed() {
        return avgSpeed;
    }

    public float getAvgPace() {
        return avgPace;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public int getCalories() {
        return calories;
    }

    public ArrayList<RunStat> getRunStats() {
        return runStats;
    }

    public float avgSpeed() {
        return calcAverage(distance, duration / SECONDS_FACTOR);
    }

    public float avgPace() {
        return calcAverage(duration / MINUTES_FACTOR, distance / MILES_FACTOR);
    }

    public RunSummary(long id, long distance, int elevation, long duration,
                      long startTime, float avgSpeed, float avgPace,
                      float maxSpeed, int calories) {
        this.id = id;
        this.distance = distance;
        this.elevation = elevation;
        this.duration = duration;
        this.startTime = startTime;
        this.avgSpeed = avgSpeed;
        this.avgPace = avgPace;
        this.maxSpeed = maxSpeed;
        this.calories = calories;
    }

    public RunSummary(long distance, int elevation, long duration, long startTime,
                      float avgSpeed, float avgPace, float maxSpeed, int calories,
                      ArrayList<RunStat> runStats) {
        this.distance = distance;
        this.elevation = elevation;
        this.duration = duration;
        this.startTime = startTime;
        this.avgSpeed = avgSpeed;
        this.avgPace = avgPace;
        this.maxSpeed = maxSpeed;
        this.calories = calories;
        this.runStats = runStats;
    }

    public RunSummary(long distance, int elevation, long duration,
                      long startTime, ArrayList<RunStat> runStats) {
        this.distance = distance;
        this.elevation = elevation;
        this.duration = duration;
        this.startTime = startTime;
        this.runStats = runStats;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.distance);
        dest.writeInt(this.elevation);
        dest.writeLong(this.duration);
        dest.writeLong(this.startTime);
        dest.writeFloat(this.avgSpeed);
        dest.writeFloat(this.avgPace);
        dest.writeFloat(this.maxSpeed);
        dest.writeInt(this.calories);
        dest.writeList(this.runStats);
    }

    protected RunSummary(Parcel in) {
        this.distance = in.readLong();
        this.elevation = in.readInt();
        this.duration = in.readLong();
        this.startTime = in.readLong();
        this.avgSpeed = in.readFloat();
        this.avgPace = in.readFloat();
        this.maxSpeed = in.readFloat();
        this.calories = in.readInt();
        this.runStats = in.readArrayList(RunStat.class.getClassLoader());
    }

    public static final Creator<RunSummary> CREATOR = new Creator<RunSummary>() {
        public RunSummary createFromParcel(Parcel source) {
            return new RunSummary(source);
        }

        public RunSummary[] newArray(int size) {
            return new RunSummary[size];
        }
    };

    public void save(Context context) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        this.id = dbHelper.addSummary(this);
    }

    public void delete(Context context) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        dbHelper.deleteSummary(id);
    }

    private float calcAverage(float numerator, float denominator) {
        if (denominator > 0)
            return numerator / denominator;
        return 0;
    }
}
