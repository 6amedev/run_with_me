package rwm.com.runwithme;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import rwm.com.runwithme.model.RunSummary;
import rwm.com.runwithme.viewmodel.RunSummaryViewModel;

public class RunSummaryActivity extends AppCompatActivity {

    public static final String RUN_SUMMARY_KEY = "run_summary_key";

    private TextView durationView;
    private TextView distanceView;
    private TextView caloriesView;
    private TextView elevationView;
    private TextView startTimeView;
    private TextView avgSpeedView;
    private TextView avgPaceView;
    private TextView maxSpeedView;
    private RunSummaryViewModel runSummaryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_summary);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.run_summary);

        findViews();
        summarize();
        displaySummary();

        LineChart chart = (LineChart) findViewById(R.id.chart_stats);
        LineData data = new LineData(getXAxisValues(), getDataSet());
        chart.setData(data);
        chart.setDescription("My Chart");
        chart.animateX(2000);
        chart.invalidate();

        overridePendingTransition(R.anim.slide_in_right, R.anim.zoom_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.discard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.discard_run:
                discardSummary();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.zoom_in, R.anim.slide_out_right);
    }

    private void findViews() {
        durationView = ((TextView) findViewById(R.id.summary_duration));
        distanceView = (TextView) findViewById(R.id.summary_distance);
        caloriesView = (TextView) findViewById(R.id.summary_calories);
        startTimeView = (TextView) findViewById(R.id.summary_start_time);
        avgSpeedView = (TextView) findViewById(R.id.summary_avg_speed);
        avgPaceView = (TextView) findViewById(R.id.summary_avg_pace);
        maxSpeedView = (TextView) findViewById(R.id.summary_max_speed);
        elevationView = (TextView) findViewById(R.id.summary_elevation);
    }

    private void summarize() {
        RunSummary summary = getIntent().getExtras().getParcelable(RUN_SUMMARY_KEY);
        RunSummary runSummary = new RunSummary(summary.getDistance(), summary.getElevation(),
                summary.getDuration(), summary.getStartTime(), summary.avgSpeed(), summary.avgPace(), 0, 0, summary.getRunStats());
        runSummaryViewModel = new RunSummaryViewModel(runSummary, getResources());
        runSummaryViewModel.saveSummary(this);
    }

    private void discardSummary() {
        runSummaryViewModel.deleteSummary(this);
        Toast.makeText(this, R.string.summary_discarded, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    private void displaySummary() {
        durationView.setText(runSummaryViewModel.timerFormatDuration());
        distanceView.setText(String.valueOf(runSummaryViewModel.getDistance()));
        caloriesView.setText(String.valueOf(runSummaryViewModel.getCalories()));
        startTimeView.setText(String.valueOf(DateFormat.getTimeInstance().format(new Date(runSummaryViewModel.getStartTime()))));
        avgSpeedView.setText(String.format("%.2f", runSummaryViewModel.getAvgSpeed()));
        avgPaceView.setText(String.format("%.2f", runSummaryViewModel.getAvgPace()));
        maxSpeedView.setText(String.format("%.2f", runSummaryViewModel.getMaxSpeed()));
        elevationView.setText(String.valueOf(runSummaryViewModel.getElevation()));
    }

    private ArrayList<LineDataSet> getDataSet() {
        ArrayList<LineDataSet> dataSets = null;

        ArrayList<Entry> valueSet1 = new ArrayList<>();

        long minutes = runSummaryViewModel.getMinutes();
        for (int minute = 1; minute <= minutes; minute++) {
            valueSet1.add(new Entry(runSummaryViewModel.getRunStats().get(minute).getDistance(), minute));
        }

        ArrayList<Entry> valueSet2 = new ArrayList<>();
        for (int minute = 1; minute <= minutes; minute++) {
            long intervalDistance;
            if (minute == 1)
                intervalDistance = runSummaryViewModel.getRunStats().get(minute).getDistance();
            else {
                intervalDistance = runSummaryViewModel.getRunStats().get(minute).getDistance() - runSummaryViewModel.getRunStats().get(minute - 2).getDistance();
            }
            float intervalAvgSpeed = intervalDistance / 60.0F;
            valueSet2.add(new Entry(intervalAvgSpeed, minute));
        }

        LineDataSet lineDataSet1 = new LineDataSet(valueSet1, "Brand 1");
        lineDataSet1.setColor(Color.rgb(0, 155, 0));
        LineDataSet lineDataSet2 = new LineDataSet(valueSet2, "Brand 2");
        lineDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(lineDataSet1);
        dataSets.add(lineDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        for (int minute = 1; minute <= runSummaryViewModel.getMinutes(); minute++) {
            xAxis.add(String.valueOf(minute));
        }
        return xAxis;
    }
}
