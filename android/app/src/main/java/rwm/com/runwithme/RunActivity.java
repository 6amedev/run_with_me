package rwm.com.runwithme;

import android.app.ActivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

public class RunActivity extends AppCompatActivity
        implements RunFragment.RunFragmentCallback,
        MapFragment.MapFragmentCallback {

    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);

        showRun();
    }

    @Override
    public void onBackPressed() {
        if (currentFragment.getClass() == MapFragment.class)
            toggleShowRun();
        else if (isRunServiceRunning())
            ActivityCompat.finishAffinity(this);
        else
            onBackPressed();
    }

    public void showRun() {
        currentFragment = new RunFragment();
        Bundle args = new Bundle();
        args.putBoolean(RunFragment.RUNNING_KEY, isRunServiceRunning());
        currentFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, currentFragment)
                .commit();
    }

    @Override
    public void toggleShowMap() {
        currentFragment = new MapFragment();
        Bundle args = new Bundle();
        args.putBoolean(RunFragment.RUNNING_KEY, isRunServiceRunning());
        currentFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.container, currentFragment)
                .commit();
    }

    @Override
    public void toggleShowRun() {
        currentFragment = new RunFragment();
        Bundle args = new Bundle();
        args.putBoolean(RunFragment.RUNNING_KEY, isRunServiceRunning());
        currentFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.container, currentFragment)
                .commit();
    }

    private boolean isRunServiceRunning() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServices) {
            if (runningServiceInfo.service.getClassName().equals(RunService.class.getName())) {
                return true;
            }
        }
        return false;
    }
}
