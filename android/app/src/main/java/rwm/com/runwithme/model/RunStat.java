package rwm.com.runwithme.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RunStat implements Parcelable {
    float pace;
    float speed;
    long distance;
    long duration;
    long startTime;
    double latitude;
    double longitude;
    int elevation;
    float accuracy;

    public RunStat(float pace, float speed, long distance,
                   long duration, long startTime, double latitude,
                   double longitude, int elevation, float accuracy) {
        this.pace = pace;
        this.speed = speed;
        this.distance = distance;
        this.duration = duration;
        this.startTime = startTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
        this.accuracy = accuracy;
    }

    public float getPace() {
        return pace;
    }

    public float getSpeed() {
        return speed;
    }

    public long getDistance() {
        return distance;
    }

    public long getDuration() {
        return duration;
    }

    public long getStartTime() {
        return startTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getElevation() {
        return elevation;
    }

    public float getAccuracy() {
        return accuracy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.pace);
        dest.writeFloat(this.speed);
        dest.writeLong(this.distance);
        dest.writeLong(this.duration);
        dest.writeLong(this.startTime);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeInt(this.elevation);
        dest.writeFloat(this.accuracy);
    }

    protected RunStat(Parcel in) {
        this.pace = in.readFloat();
        this.speed = in.readFloat();
        this.distance = in.readLong();
        this.duration = in.readLong();
        this.startTime = in.readLong();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.elevation = in.readInt();
        this.accuracy = in.readFloat();
    }

    public static final Creator<RunStat> CREATOR = new Creator<RunStat>() {
        public RunStat createFromParcel(Parcel source) {
            return new RunStat(source);
        }

        public RunStat[] newArray(int size) {
            return new RunStat[size];
        }
    };
}
