package rwm.com.runwithme.viewmodel;

import android.content.res.Resources;

import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.util.Date;

import rwm.com.runwithme.R;
import rwm.com.runwithme.model.RunSummary;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RunSummaryViewModelTest {

    private RunSummaryViewModel runSummaryViewModel;
    private Resources resources;

    @Before
    public void setup() {
        resources = mock(Resources.class);
        when(resources.getString(R.string.ago)).thenReturn("ago");
        when(resources.getString(R.string.unit_minute)).thenReturn("minute");
        when(resources.getString(R.string.unit_hour)).thenReturn("hour");
        when(resources.getString(R.string.unit_day)).thenReturn("day");
        when(resources.getString(R.string.unit_meter)).thenReturn("m");
        when(resources.getString(R.string.unit_calorie)).thenReturn("cal");
    }

    @Test
    public void shouldReturnTimeForRunJustFinished() {
        RunSummary runSummary = new RunSummary(0, 0, 0, new Date().getTime(), null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("Just now", runSummaryViewModel.timeElapsed());
    }

    @Test
    public void shouldReturnTimeForRunFinishedAMinuteAgo() {
        RunSummary runSummary = new RunSummary(0, 0, 0, new Date().getTime() - 60000, null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("1 minute ago", runSummaryViewModel.timeElapsed());
    }

    @Test
    public void shouldReturnTimeForRunFinishedFewHoursAgo() {
        RunSummary runSummary = new RunSummary(0, 0, 0, new Date().getTime() - 9000000, null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("2 hours ago", runSummaryViewModel.timeElapsed());
    }

    @Test
    public void shouldReturnTimeForRunFinishedLessThanThreeDaysAgo() {
        RunSummary runSummary = new RunSummary(0, 0, 0, new Date().getTime() - 180800000, null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("2 days ago", runSummaryViewModel.timeElapsed());
    }

    @Test
    public void shouldReturnTimeForRunFinishedMoreThanThreeDaysAgo() {
        long startTime = new Date().getTime() - 350000000;
        RunSummary runSummary = new RunSummary(0, 0, 0, startTime, null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        String expectedTime = DateFormat.getDateInstance().format((new Date(startTime)));
        assertEquals(expectedTime, runSummaryViewModel.timeElapsed());
    }

    @Test
    public void shouldReturnDistanceWithUnits() {
        RunSummary runSummary = new RunSummary(1029, 0, 0, 0, null);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("1029 m", runSummaryViewModel.getDistanceText());
    }

    @Test
    public void shouldReturnCaloriesWithUnits() {
        RunSummary runSummary = new RunSummary(0, 0, 0, 0, 0, 0, 0, 0, 245);
        runSummaryViewModel = new RunSummaryViewModel(runSummary, resources);
        assertEquals("245 cal", runSummaryViewModel.getCaloriesText());
    }
}