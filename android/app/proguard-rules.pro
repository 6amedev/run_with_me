# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/ironman/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes Signature
-keepattributes Exceptions

-keep class * extends android.app.Application {
    *;
}

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep public class com.github.mikephil.charting.animation.* {
    public protected *;
}

# parse
-allowaccessmodification
-dontwarn com.parse.ParseTwitterUtils
-dontwarn com.squareup.**
-keep class com.squareup.** { *; }
-dontwarn com.parse.ParseOkHttpClient**
-keep class com.parse.ParseOkHttpClient** { *; }
-keep class org.parceler.Parceler$$Parcels
-dontwarn android.net.SSLCertificateSocketFactory
-dontwarn android.net.SSLCertificateSocketFactory
-dontwarn com.parse.NotificationCompat**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.parse.twitter.*