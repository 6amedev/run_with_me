class RunsController < ApplicationController
  before_action :set_run, only: [:show, :destroy]

  def index
    @runs = Run.all
  end

  def show
  end

  def create
    @run = Run.new(run_params)

    respond_to do |format|
      if @run.save
        format.json { render :show, status: :created, location: @run }
      else
        format.json { render json: @run.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @run.delete
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  def set_run
    @run = Run.find(params[:id])
  end

  def run_params
    params.require(:run).permit(:duration, :distance, :calories, :max_speed, :elevation_gain, :avg_speed, :avg_pace)
  end
end
