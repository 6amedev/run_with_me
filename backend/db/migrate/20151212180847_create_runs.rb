class CreateRuns < ActiveRecord::Migration
  def change
    create_table :runs do |t|
      t.integer :duration
      t.integer :distance
      t.integer :calories
      t.float :max_speed
      t.integer :elevation_gain

      t.timestamps null: false
    end
  end
end
