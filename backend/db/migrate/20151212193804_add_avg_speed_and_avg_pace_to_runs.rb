class AddAvgSpeedAndAvgPaceToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :avg_speed, :float
    add_column :runs, :avg_pace, :float
  end
end
