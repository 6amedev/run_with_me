# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Run.delete_all

Run.create([
              {duration: 80, distance: 600, calories: 24, max_speed: 10, elevation_gain: 5, avg_speed: 7.5, avg_pace: 3.57},
              {duration: 100, distance: 900, calories: 40, max_speed: 13, elevation_gain: 0, avg_speed: 9, avg_pace: 2.98}
          ])
